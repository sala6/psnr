SHELL := /bin/bash

RNASEQ = $(wildcard input/gdac.*.tar.gz)

# Download, verify and extract all files present in MANIFEST.json,
# Then add them into a single sqlite DB
database: primes rnaseq hgnc intact

primes: mtKRAS-Hi.csv
	./scripts/import/primes.sh

rnaseq: COADREAD_rnaseq_ga.tar.gz COADREAD_rnaseq_hiseq.tar.gz
	./scripts/import/rnaseq.sh

hgnc: hgnc_ids.tsv
	./scripts/import/hgnc.py

intact: intact.zip
	./scripts/import/intact.sh

FILES = intact.zip mtKRAS-Hi.csv COADREAD_rnaseq_ga.tar.gz COADREAD_rnaseq_hiseq.tar.gz hgnc_ids.tsv

download: MANIFEST.json
	./scripts/download.py

$(FILES): MANIFEST.json
	./scripts/download.py $@

# This step converts the RNAseq data into a bunch of lists...
# And it also does a bunch of thresholdy things...
# This step sucks and is bad. I'm going to replace this with my shiny new database.
output/lists: $(RNASEQ)
	LIST="" ;\
	for file in $^; do \
		tar -xf $$file -C input/ ;\
		f=$$file ;\
		LIST+=" $${f%.tar.gz}/COADREAD*.txt" ;\
	done ;\
	mkdir -p output/lists ;\
	./scripts/rnaseq_to_lists.R $$LIST

output/sifs: input/human.zip
	mkdir -p input/xml
	unzip input/human.zip -d input/xml/

output/sifs/union.sif: output/sifs
	cd output/sifs ;\
	rm union.sif ;\
	touch union.sif ;\
	for file in *.sif; do \
		if [[ $$file = *"union"* ]]; then \
			continue ;\
		fi ;\
		echo "Now adding" "$$file" ;\
		sifter union union.sif "$$file" > temp ;\
		cp temp union.sif ;\
	done ;\
	rm temp

output/nets: output/combined.sif output/lists
	mkdir -p output/nets
	for list in output/lists/*.txt ;\
	do \
		sifter remove $$list output/combined.sif > "output/nets/$$(basename "$${list}.sif")" ;\
	done ;\


output/baits_rg:
	./scripts/baits_to_eg.R

output/combined.sif: output/output.sif output/baits_rg
	./scripts/primes_to_eg.R
	sifter remove output/baits_eg output/output.sif > output/temp
	sifter overlay output/temp output/primes_eg.sif > output/combined.sif
	rm output/temp

output/output.sif: output/sifs/union.sif
	./scripts/refseq_to_entrez.R

stats:
	mkdir -p output/rewired
	./scripts/rewired.sh
	./scripts/sumstats.R

sifter:
	git submodule init
	git submodule update
	cd rsif ;\
	cargo build --release ;\
	cargo install --force

clean:
	rm -rf output/*
