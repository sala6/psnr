#!/usr/bin/env python3

# Import imex/intact data into sqlite
# This script is not in use, but I'm keeping it around as an example

import pandas

print('reading')
df = pandas.read_csv('input/intact.txt', encoding='utf8', sep='\t', dtype=str)

print('done reading')
# do things here...
df.rename(columns={'#ID(s) interactor A':'uniprot_a', 'ID(s) interactor B':'uniprot_b', 'Interaction detection method(s)':'method', 'Interaction identifier(s)':'interaction_id'}, inplace=True)
print('done renaming')
df = df.loc[:, ['interaction_id', 'method', 'uniprot_a', 'uniprot_b']]
df = df[(df.uniprot_a.str.contains("uniprotkb") & df.uniprot_b.str.contains("uniprotkb"))] # filter out rows without two uniprot ids interacting
print('done filtering rows')
df.interaction_id.replace('(intact:)(EBI\-\d*)(.*)', '\\2', regex=True, inplace=True) # retain only intact accession id, eg. EBI-XXXXXX
df.uniprot_a.replace('(uniprotkb:)(\w*)(.*)', '\\2', regex=True, inplace=True) # retain only uniprot
df.uniprot_b.replace('(uniprotkb:)(\w*)(.*)', '\\2', regex=True, inplace=True) # retain only uniprot
df.method.replace('(psi-mi:\")(MI:\d*)(.*)', '\\2', regex=True, inplace=True) # get method used in MI id

#subset.to_sql('intact', conn, if_exists='replace', index=False)

#conn.commit()
#conn.close()

#print('IntAct PPI table imported')

