#!/bin/bash	
unzip input/intact.zip -d input/
echo 'uniprot_a,uniprot_b,intact_id' > input/human_intact.csv
xsv select "#ID(s) interactor A","ID(s) interactor B","Interaction identifier(s)","Host organism(s)" -d '\t' input/intact.txt | # Open file, tab delimited, select colummns by name
	grep taxid:9606 | # Filter out non-human interaction
	sed 's/taxid.*//' | # Delete everything after taxid
	cut -d "|" -f1 | # Remove any unwanted interaction identifiers (intact is always first, others may follow after an '|')
	grep 'uniprotkb.*uniprotkb' | # Filter out interactions with no uniprot
	sed 's/"//g' | # Get rid of redundant stuff
	sed 's/uniprotkb://g' | 
	sed 's/intact://' | 
	sed 's/,*$//g'  >> input/human_intact.csv
sqlite3 output/psnr.db \
	'.timeout 10000' \
	'.mode csv' \
	'drop table if exists intact' \
	'.import input/human_intact.csv intact'
echo "intact table written to database"
