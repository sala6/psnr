#!/bin/bash	
set -e # stop if anything fails

# Rename TCGA files because they're really long
for file in input/COADREAD_rnaseq_*.tar.gz; do \
	DIR=${file%.tar.gz} ;\
	mkdir -p $DIR ;\
	tar -xf $file -C $DIR --strip-components 1 ;\
	rm $DIR/MANIFEST.txt ;\
	sed '2d' $DIR/*.txt |
	sed '1s/Hybridization REF/symbol	entrez/' | 
	sed 's/|/	/' > $DIR/rnaseq.tsv ;\
done ;\

sqlite3 output/psnr.db \
	'.timeout 10000' \
	'.mode tabs' \
	'drop table if exists rnaseq_ga' \
	'.import input/COADREAD_rnaseq_ga/rnaseq.tsv rnaseq_ga' \
	'drop table if exists rnaseq_hiseq' \
	'.import input/COADREAD_rnaseq_hiseq/rnaseq.tsv rnaseq_hiseq'
echo "rnaseq tables written to database"
