#!/bin/bash	

xsv select "Bait","Bait_UniProt","Prey","Prey_UniProt" input/mtKRAS-Hi.csv > input/mtkrashi_filtered.csv

sqlite3 output/psnr.db \
	'.timeout 10000' \
	'.mode csv' \
	'drop table if exists mtkras_hi' \
	'.import input/mtkrashi_filtered.csv mtkras_hi'
echo "primes mtkras_hi written to database"
