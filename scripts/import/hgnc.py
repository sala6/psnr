#!/usr/bin/env python3

# Import hgnc table to sqlite database

import sqlite3
import pandas

conn = sqlite3.connect('output/psnr.db')
df = pandas.read_csv('input/hgnc_ids.tsv', encoding='utf8', sep='\t', dtype=str)

# do things here...
df.rename(columns={'HGNC ID':'hgnc_id', 'Approved Symbol':'symbol', 'Approved Name':'name', 'Entrez Gene ID(supplied by NCBI)':'entrez', 'Ensembl Gene ID':'ensembl', 'RefSeq IDs':'refseq', 'UniProt ID(supplied by UniProt)':'uniprot'}, inplace=True)
subset = df.loc[:, ['hgnc_id', 'symbol', 'name', 'entrez', 'ensembl', 'refseq', 'uniprot']]
subset.to_sql('hgnc', conn, if_exists='replace', index=False)

conn.commit()
conn.close()

print('HGNC conversion table imported')

