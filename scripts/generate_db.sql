.mode tabs
drop table if exists hgnc;
.import input/hgnc_ids.tsv hgnc
drop table if exists rnaseq_ga;
.import input/COADREAD_rnaseq_ga/rnaseq.tsv rnaseq_ga
drop table if exists rnaseq_hiseq;
.import input/COADREAD_rnaseq_hiseq/rnaseq.tsv rnaseq_hiseq

.mode csv
drop table if exists intact;
.import input/human_intact.csv intact
drop table if exists mtkras_hi;
.import input/mtkras_filtered.csv mtkras_hi
