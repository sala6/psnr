import json
import gzip
import pandas as pd

with open('metadata.cart.2018-11-21.json') as f:
    metadata = json.load(f)

frame = pd.DataFrame()
for entry in metadata:
    directory = entry['file_id']
    file_name = entry['file_name']
    md5sum = entry['md5sum']
    tcga_id = entry['associated_entities'][0]['entity_submitter_id']
    # files appear to be tab delimited
    with gzip.open('download/' + directory + '/' + file_name, 'r') as file:
       sample = pd.read_csv(file, sep='\t', index_col=0, header=None)
       sample.columns = [tcga_id]
       frame = pd.concat([frame, sample], axis=1)
    #print(json.dumps(entry, indent=4))
    #

frame.to_csv('htseq_raw.csv')
