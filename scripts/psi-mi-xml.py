#!/usr/bin/env python2.7
import xml.etree.cElementTree as ET
import sys


ns = "{http://psi.hupo.org/mi/mif}"

xml = ET.parse(sys.argv[1])
entrySet = xml.getroot()

interactor_map = {}
unidentifiable = 0;

n = 0
interaction_count = 0

for e in entrySet: # iterate db entries

    name = e.find(ns + 'source').find(ns + 'names').find(ns + 'shortLabel').text

    interactors = e.find(ns + 'interactorList')
    for i in interactors:
        interactor_map[i.attrib['id']] = 0
        identified = False
        for ref in i.find(ns + 'xref').iter():
            if ref.get('db') == "refseq":
                identified = True
                interactor_map[i.attrib['id']] = ref.get('id')
                break
            elif ref.get('db') == "intact":
                identified = True
                interactor_map[i.attrib['id']] = ref.get('id')
                break
        if identified == False:
            unidentifiable += 1

    interactions = e.find(ns + 'interactionList')
    for i in interactions: # iterate interactions
        interaction_count += 1
        i_type = i.find(ns + 'interactionType').find(ns + 'names').find(ns + 'shortLabel').text
        
        # DISCARD INTERACTIONS THAT ARE NOT PHYISCAL ASSOCIATIONS
        #if i_type != "physical association":
        #    n += 1
        #    print(i_type) 
        #    continue

        participants = i.find(ns + 'participantList')
        name = i.find(ns + 'names').find(ns + 'shortLabel').text

        bait = []
        prey = []
        n += 0
        
        for p in participants:
            role = p.find('.//' + ns + 'experimentalRole')
            rt = role.find(ns + 'names').find(ns + 'shortLabel').text
            if rt == 'bait':
                bait.append(p)
            elif rt == 'prey':
                prey.append(p)

        if len(bait) == 1 and len(prey) > 0:
            for i in range(0, len(prey)):
                n += 1
                print(interactor_map[bait[0].find(ns + 'interactorRef').text] + '\t' + name + '\t' + interactor_map[prey[i].find(ns + 'interactorRef').text])


