#!/bin/bash

count=$(sifter nodes output/combined.sif | wc -l)
for file in output/nets/*.sif
do
	pscount=$(sifter nodes "$file" | wc -l)
	rewired=$((count - pscount))
	echo "$rewired"
done
