#!/usr/bin/env python

# Checks all files required by the manifest, downloads if needed.

import json, urllib.request, os, sys, hashlib, multiprocessing
from multiprocessing import Pool

required = sys.argv[1:]

def is_valid(item):
    md5 = item["md5"]
    check = hashlib.md5(open("input/" + item["name"], "rb").read()).hexdigest()
    if md5 == check:
        print(item["name"] + " verified!")
        return True
    else:
        print(item["name"] + ": Verification error! Expected md5: " + md5 + " Found: " + check)
        return False

def download(item):
    url = item["address"]
    print("Now downloading " +  item["name"])
    response = urllib.request.urlopen(url)
    
    with open("input/" + item["name"], "wb") as file:
        file.write(response.read())

def get_item(item):
    filename = "input/" + item["name"]
    if os.path.exists(filename):
        if is_valid(item):
            return
    else:
        download(item)
        is_valid(item)

with open('MANIFEST.json') as f:
    data = json.load(f)
    if len(required) > 0:
        data["manifest"] = [item for item in data["manifest"] if item["name"] in required]

if __name__ == '__main__':    
    with Pool(8) as p:
        p.map(get_item, data["manifest"])
