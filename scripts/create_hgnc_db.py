#!/usr/bin/env python3

import sqlite3
import pandas
import io
import urllib.request

conn = sqlite3.connect('hgnc.db')

with urllib.request.urlopen('https://www.genenames.org/cgi-bin/download?col=gd_hgnc_id&col=gd_app_sym&col=gd_app_name&col=gd_status&col=gd_prev_sym&col=gd_aliases&col=gd_pub_chrom_map&col=gd_pub_acc_ids&col=gd_pub_eg_id&col=gd_pub_ensembl_id&col=gd_pub_refseq_ids&col=md_prot_id&status=Approved&status=Entry+Withdrawn&status_opt=2&where=&order_by=gd_app_sym_sort&format=text&limit=&submit=submit') as response:
    data = response.read()
    df = pandas.read_csv(io.BytesIO(data), encoding='utf8', sep='\t')
    df.to_sql("hgnc", conn, if_exists="replace", index=False)
     

conn.commit()
conn.close()
