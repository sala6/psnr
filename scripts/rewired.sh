#!/bin/bash

mkdir output/rewired
for file in output/lists/*.txt
do
	comm -12 <(sort $file) <(sifter nodes output/combined.sif | sort) > output/rewired/$(basename "$file")
done
