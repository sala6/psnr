#!/usr/bin/env python3

from sys import argv
import pickle

# Get arguments
mode = argv[1]
refid = argv[2]

# Deserialize map
f = open('xref.p', 'rb')
xref = pickle.load(f)
f.close()

entrez = xref["entrez"]
uniprot = xref["uniprot"]
refseq = xref["refseq"]
ena = xref["ena"]

if mode == 'entrez':
    print(entrez[refid])
elif mode == 'uniprot':
    print(uniprot[refid])
elif mode == 'refseq':
    print(refseq[refid])
elif mode == 'ena':
    print(ena[refid])
else:
    print('wadafaq')


