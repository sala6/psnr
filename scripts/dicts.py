#!/usr/bin/env python3

import pandas
import pickle

prefix="Homo_sapiens.GRCh38.93."
filenames=["entrez", "uniprot", "refseq", "ena"]
colnames=["gene_stable_id", "transcript_stable_id", "protein_stable_id", "xref"]

xref_to_transcript_id = {} # A dict that contains other dicts

for name in filenames:
    data = pandas.read_csv(prefix+name+'.tsv', usecols=colnames, sep='\t')
    
    xref = {}
    for row in data.itertuples():
        if row.xref in xref:
            xref[row.xref].add(row.gene_stable_id)
        else:
            xref[row.xref] = {row.gene_stable_id}

    xref_to_transcript_id[name] = xref

f = open('xref.p', 'wb')
pickle.dump(xref_to_transcript_id, f, -1)
f.close()
