#!/usr/bin/env Rscript

library(org.Hs.eg.db)
library(annotate)

union <- read.csv(file="output/sifs/union.sif", sep='\t', header=F, stringsAsFactors=F)
union[,1] <- gsub('\\..*', '', union[,1]) # remove version bit
union[,3] <- gsub('\\..*', '', union[,3]) # remove version bit

col1 <- ifelse(startsWith(union[,1], "EBI"), union[,1], lookUp(union[,1], 'org.Hs.eg', 'REFSEQ2EG'))
col3 <- ifelse(startsWith(union[,3], "EBI"), union[,3], lookUp(union[,3], 'org.Hs.eg', 'REFSEQ2EG'))

col1n = ifelse(is.na(col1), union[,1], col1)
col3n = ifelse(is.na(col3), union[,3], col3)

output <- sapply(1:length(col1n), function(x) sprintf("%s\t-\t%s", col1n[x], col3n[x]))
fileConn<-file("output/output.sif")
writeLines(output, fileConn)
close(fileConn)
